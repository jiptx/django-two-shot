from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def show_Receipt(request):
    Receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"Receipt_list": Receipt_list}
    return render(request, "home.html", context)


@login_required
def create_Receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save()
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = ReceiptForm()
        context = {
            "form": form,
        }
    return render(request, "create.html", context)


@login_required
def ExpenseCategoryList(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


@login_required
def AccountList(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


@login_required
def create_ExpenseCategory(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")

    else:
        form = CategoryForm()
        context = {
            "form": form,
        }
    return render(request, "categories/create.html", context)


@login_required
def create_Account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("account_list")

    else:
        form = CategoryForm()
        context = {
            "form": form,
        }
    return render(request, "accounts/create.html", context)


# Create your views here.
