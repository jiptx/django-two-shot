from django.urls import path
from receipts.views import (
    show_Receipt,
    create_Receipt,
    ExpenseCategoryList,
    AccountList,
    create_ExpenseCategory,
    create_Account,
)


urlpatterns = [
    path("", show_Receipt, name="home"),
    path("create/", create_Receipt, name="create_receipt"),
    path("categories/", ExpenseCategoryList, name="category_list"),
    path("accounts/", AccountList, name="account_list"),
    path("categories/create/", create_ExpenseCategory, name="create_category"),
    path("accounts/create/", create_Account, name="create_account"),
]
